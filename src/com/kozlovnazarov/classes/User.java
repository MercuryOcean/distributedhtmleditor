package com.kozlovnazarov.classes;

public class User {

    private String _name;

    public User(String name){
        _name = name;
    }

    public String getName(){
        return _name;
    }

    public void setName(String name){
        _name = name;
    }
}
