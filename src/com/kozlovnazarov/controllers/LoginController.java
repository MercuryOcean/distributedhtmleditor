package com.kozlovnazarov.controllers;

import javafx.event.ActionEvent;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController {

    @FXML
    private TextField nameField;

    public void enterName(ActionEvent actionEvent) {
        try{
            String name = nameField.getText();
            if (name != "" && name != null) {
                Parent root = FXMLLoader.load(getClass().getResource("..\\sample.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Editor");
                stage.setScene(new Scene(root, 966, 535));
                Scene scene = stage.getScene();

                Label nameLabel = (Label) scene.lookup("#userName");
                nameLabel.setText(name);

                ListView listView = (ListView<String>) scene.lookup("#RawList");
                listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                Path htmlDocPath = Paths.get("src/com/kozlovnazarov/document.html");
                Charset charset = Charset.forName("UTF-8");
                List<String> lines = Files.readAllLines(htmlDocPath, charset);
                for (String line : lines) {
                    System.out.println(line);
                }
                ObservableList<String> list = (ObservableList<String>) FXCollections.observableArrayList(lines);
                listView.setItems(list);

                stage.show();
                ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
            }
        }
        catch(IOException e){
            e.printStackTrace();
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }
}
